Previous research on the use of gaze-based graphical keys has relied on manual
activation when selecting password points. This has been based either on
dwell-time activation (users resting their eyes on an image point for a few
seconds) or activation of a physical button to manually determine the inclusion.
While these methods tend to strengthen certain security aspect compared to
physical-button keypads\footnotemark with regard to shoulder surfing
attacks\footnotemark , they still lack in the sense that they provide visual
clues for potential attackers. In this paper we describe a different approach;
nondiscrete graphical passwords. Instead of picking a discrete set of points in
an image, users choose a continuous walk over the image. The difference between
manual- and nondiscrete- selection is visualized in figure
\ref{fig:non-vs-discrete}. It is worth noting that nondiscrete passwords are
discrete in the sense that digital images are discrete, but we can abstract that
fact, by removing manual selection, and using comparison algorithms that treat
passwords as being discrete. Nondiscrete keys are analogous to drawing on top of
a keypad, rather than pressing buttons separately.

\footnotetext[1]{As seen in most ATM machines}
\footnotetext[2]{A type of a attack where the password is obtained by observing
legitimate users as they log in}

\begin{figure}
	\center
	\includestandalone[scale=0.88]{images/continuous_vs_manual_selection_diagram}
	\caption[caption]{
		\textbf{Left:} A gaze-based password (black line) annotated with
		manually selected password points (black circles) and the
		password points accepted by dwell-time based selection (blue
		squares).
		\\
		\textbf{Right:} The same password as to the left (black line),
		but with the password points accepted by nondiscrete selection
		(blue squares).
	}
	\label{fig:non-vs-discrete}
\end{figure}


\subsubsection{Authenticating nondiscrete passwords}
To authenticate nondiscrete locimetric passwords, we need a password metric that
is robust enough to ignore imprecisions of both the gaze tracker and the users,
while stable enough to avoid false positives. In this project, we use a fast
implementation of the \frechet{} distance, adapted for comparison of polygons
\cite{EM94}. 

The \frechet{} distance is canonically described as the length of the shortest
leash that could connect a dog with its owner, as they each walk their own path
(equivalent or not). It is required that both the dog and owner walk the full length
of their paths, and that they never backtrack. The general characteristics of the
\frechet{} distance have been well described by
\cite{AltG95} and \cite{EM94}.

To formally define the \frechet{} distance between two keys $P,Q$, we
parameterize the key pair with two functions $\alpha(t), \beta(t)$, that define
the walks over the keys. $\alpha(t)$ and $\beta(t)$ are non-decreasing functions
of time ($t$), to all points in $P$ or $Q$ respectively. The property of being
non-decreasing corresponds to "not backtracking" in the previous definition. The
\frechet{} distance between $P$ and $Q$, $\delta_{F}(P,Q)$ is then defined as:
\begin{equation}
\delta_{F}(P,Q) = \inf_{\alpha,\beta} \left\{\max_{t} \Big(|\alpha(t)-
\beta(t)|\Big) \right\}
\end{equation}

To accommodate our discrete definition of coordinates in a key, we use the
\texttt{Eiter and Mannila} implementation of the \frechet{} distance, as it
works on discrete sets of points, rather than continuous curves. The most
important characteristics of their implementation are:

\begin{LaTeXdescription} \item[No underestimation:] The implementation will
		never return a smaller distance than the true \frechet{}
	distance. \item[Bounded overestimation:] The implementation
		overestimates the true \frechet{} distance by at most the
		longest distance between two consecutive points, in either the
password, or the password attempt. \end{LaTeXdescription}

The \frechet{} distance provides a robust but stable metric. It achieves
robustness, with regard to input error, by considering an optimistic
interpretation of a login attempt (optimal walk of the password pairs), and
stability, with regard to false positives, by considering the pessimistic case
(largest distance) of that interpretation.

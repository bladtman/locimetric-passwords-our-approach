inputFile = article.tex
auxFile = article.aux
bibTmpFile = article.bbl
outputFile = article.pdf
tmpdir = tmp

$(outputFile): $(tmpdir)/$(outputFile)
	cp $< $@

$(tmpdir)/$(auxFile): $(inputFile) abstract.tex introduction.tex model.tex relatime.tex continuous.tex saliency.tex experiments/* conclusion.tex IEEEabrv.bib IEEEtran.bst bibliography.bib $(tmpdir)
	pdflatex -output-directory=$(tmpdir) --draft $<
	pdflatex -output-directory=$(tmpdir) --draft $<

$(tmpdir)/$(bibTmpFile): $(tmpdir)/$(auxFile) $(tmpdir)
	bibtex $<

$(tmpdir)/$(outputFile): $(inputFile) $(tmpdir)/$(bibTmpFile)
	pdflatex -output-directory=$(tmpdir) --draft $<
	pdflatex -output-directory=$(tmpdir) $<

$(tmpdir):
	mkdir $(tmpdir)
